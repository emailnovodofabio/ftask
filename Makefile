TARGET=		ftask
CC=			gcc -pthread
SHELL=		/bin/sh
PREFIX=		/home/fj/c/ftask
BINDIR=		$(PREFIX)
MANDIR=		$(PREFIX)
DBDIR=		/home/fj/c/ftask

CFLAGS=	 	-Wno-unused-result -Wsign-compare
INCLUDES=	-I/usr/include
LIBS=			-L/usr/lib
LDFLAGS=		-l sqlite3

OBJS=			ftask.c

ftask: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $(TARGET)

clean: ftask
	rm $(TARGET)

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>

#define VERSION "0.2"
#define VERSION_PRINTF printf("Version %s Coded %s\n", VERSION, DATE)
#define DATE "2022-11-06"
#define DB "/home/fj/c/ftask/DB_task.db"
#define README "/home/fj/c/ftask/ftask.1"

#define ENTER 10
#define ESCAPE 27
#define MAXTASKDESC 50

#define ERRCHECK {if (err!=NULL) {printf("%s\n",err); sqlite3_free(err); return 0; }}
#define DBCHECK if(rc) { \
   fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db)); return 0; }
#define max(a, b) (a > b ? a : b)

struct ftask {
   unsigned int  	id, priority, status, id_user, id_owner;
   char				description[MAXTASKDESC];
   char     		created[25], modified[25];
   int         		started, ended;
   struct 			user {
      unsigned int	id;
      char			name[30];
   } user;
};

typedef struct ftask FTASK;
typedef FTASK *FTASKPTR;

void	insert(char *);
void	list(char *);
short	delete(char *);
int 	set(char *arg1, char *arg2, char *arg3);

int		exec_query(char *);
int		select_list(int);
int 	check_task(int);
static int callback(void *data, int argc, char **argv, char **azColName);

int		print_readme(void);

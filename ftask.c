#include "ftask.h"

int main( int argc, char *argv[ ] )
{
	if( argc >= 2 )
	{
		char * arg1_char;
		long int arg1_lint;
		arg1_lint = strtol(argv[1], &arg1_char, 10);
		arg1_lint = (strlen(arg1_char) <= 0) ? arg1_lint : 0;

		if(strcmp(argv[1],"list") == 0)
		{
			if(argc <= 2)
				list("0");
		}

		else if(strcmp(argv[1],"add") == 0)
		{
			char buffer[MAXTASKDESC] = "\0";
			int argvs_len = 0;
			for(int i=2; i<argc; i++)
			{
				argvs_len += strlen(argv[i])+1;
				if( argvs_len<MAXTASKDESC )
				{	strcat(buffer, argv[i]);
					if(i+1<argc)
						strcat(buffer, " ");
				}
			}
			insert(buffer);
		}

		else if( arg1_lint > 0 )
		{
			if(strcmp(argv[2],"set") == 0)
			{
				if(strcmp(argv[3],"description") == 0)
				{
					char buffer[MAXTASKDESC] = "\0";
					int argvs_len = 0;
					for(int i=4; i<argc; i++)
					{
						argvs_len += strlen(argv[i])+1;
						if( argvs_len<MAXTASKDESC )
						{	strcat(buffer, argv[i]);
							if(i+1<argc)
								strcat(buffer, " ");
						}
					}
					set(argv[1], argv[3], buffer);
				}
				else if( argc == 5 )
					set(argv[1], argv[3], argv[4]);
				else
					printf("Many arguments to update.\n");
			}

			else if(strcmp(argv[2],"del") == 0)
			{
				if (argc == 3)
					delete(argv[1]);
				else
					printf("Many arguments to delete.\n");
			}

			else if(strcmp(argv[2],"list") == 0)
			{
				if(argc == 3)
					list(argv[1]);
			}
		}

		else if( (strcmp(argv[1],"version") == 0) && (argc <= 2) )
		{	VERSION_PRINTF;
			return 0;
		}

		else print_readme();
	}
	else print_readme();

	return 0;
}

void insert(char *arg)
{
	FTASKPTR newPtr;
	newPtr = malloc(sizeof(FTASK));

	if (newPtr != NULL)
	{
		newPtr->id = 0;
		newPtr->priority = 0;
		strcpy(newPtr->description, arg);
		newPtr->status = 0;
		char* query = sqlite3_mprintf("INSERT INTO task "\
			"(description, priority, status, created) " \
			"VALUES ('%s',1,1,datetime(CURRENT_TIMESTAMP,'localtime'));", newPtr->description);
		exec_query(query);
		sqlite3_free(query);
		free(newPtr);
	}
}

void list(char *arg)
{
	select_list(atoi(arg));
}

int select_list(int id)
{
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = sqlite3_open_v2(DB, &db, SQLITE_OPEN_READWRITE, NULL); 
	DBCHECK;

   	char* query = sqlite3_mprintf("SELECT * FROM task");
   	char* query_count = sqlite3_mprintf("SELECT COUNT(id) FROM task");
	char* query_where = sqlite3_mprintf(" WHERE id=%d;", id);
	int rowcount = 0;
	if( id > 0 )
	{
		strcat(query, query_where);
		strcat(query_count, query_where);
	}

	//printf("QRY: %s\n", query);

	/* ROW COUNT */
	sqlite3_prepare_v2(db, query_count, -1, &stmt, NULL);
	if( rc != SQLITE_OK ) return (0);
	rc = sqlite3_step(stmt);
	if( (rc == SQLITE_DONE) || (rc != SQLITE_ROW) )
		return (0);
	else
		rowcount = sqlite3_column_int(stmt, 0);
	sqlite3_finalize(stmt);
	/* -------- */

	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	//int num_cols = sqlite3_column_count(stmt);
	FTASKPTR tasks;
	if ((tasks = malloc(rowcount * sizeof(FTASK))) != NULL)
	{
		int aux = 0;
		while( sqlite3_step(stmt) != SQLITE_DONE )
		{
			tasks[aux].id = sqlite3_column_int(stmt, 0);
			strcpy(tasks[aux].description, sqlite3_column_text(stmt, 1));
			tasks[aux].priority = sqlite3_column_int(stmt, 2);
			tasks[aux].status = sqlite3_column_int(stmt, 3);
			tasks[aux].started = sqlite3_column_int(stmt, 4);
			tasks[aux].ended = sqlite3_column_int(stmt, 5);
			tasks[aux].id_user = sqlite3_column_int(stmt, 6);
			tasks[aux].id_owner = sqlite3_column_int(stmt, 7);
			strncpy(tasks[aux].created, sqlite3_column_text(stmt, 8), 16);
			if( sqlite3_column_text(stmt, 9) )
				strncpy(tasks[aux].modified, sqlite3_column_text(stmt, 9), 16);
			aux++;
		}
	}
	else {
		printf("Memory is full.\n");
		return 1;
	}

	int max_desc_len = 0;
	int max_id_len = 0;
	char buffer[8];
	int max_id = 0;
	for( int i=0; i<rowcount; i++)
	{
		max_id = max(max_id, tasks[i].id);
		if(max_desc_len < strlen(tasks[i].description))
			max_desc_len = strlen(tasks[i].description);
	}
	sprintf(buffer, "%d", max_id);
	max_id_len = strlen(buffer);
	
	printf("\nid%*s description%*s", (max_id_len-2), "", (max_desc_len-11), "");
	printf(" p s created          modified\n");
	for( int j=0; j<max_id_len; j++ ) printf("-");
	printf(" ");
	for( int k=0; k<max_desc_len; k++ ) printf("-");
	printf(" - - ---------------- ----------------\n");

	for( int i=0; i<rowcount; i++)
	{
		printf("%*d ", max_id_len, tasks[i].id);
		printf("%-*s ", max_desc_len, tasks[i].description);
		printf("%d ", tasks[i].priority);
		printf("%d ", tasks[i].status);
		printf("%s ", tasks[i].created);
		printf("%s ", tasks[i].modified);

		printf("\n");
	}
	printf("\n%d tasks.\n\n", rowcount);
	
	free(tasks);
	sqlite3_free(query);
	//sqlite3_free(query_where);
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	
	return 0;
}

short delete(char *arg)
{
	char* query = sqlite3_mprintf("DELETE FROM task WHERE id=%d;", atoi(arg));
	int result = 0;
	result = exec_query(query);
	printf("\n%d task deleted.\n\n", result);
	sqlite3_free(query);
	return result;
}

int exec_query(char *query)
{
	int affected_rows = 0;
	sqlite3 *db;
	char *zErrMsg = 0;
	const char* data = "Callback function called";
	int rc = sqlite3_open_v2(DB, &db, SQLITE_OPEN_READWRITE, NULL); 
	DBCHECK;

	rc = sqlite3_exec(db, query, callback, (void*)data, &zErrMsg);

	if(rc != SQLITE_OK)
	{	fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else
		affected_rows = sqlite3_changes(db);

//	sqlite3_free(query);
	sqlite3_close(db);

	return affected_rows;
}

FTASKPTR select_task(int id)
{
	FTASKPTR t;
	if ((t = malloc(sizeof(FTASK))) == NULL)
	{
		printf("Memory is full.\n");
		return NULL;
	}
	
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = sqlite3_open_v2(DB, &db, SQLITE_OPEN_READWRITE, NULL);
	DBCHECK;
	char* query = sqlite3_mprintf("SELECT * FROM task WHERE id=%d;", id);

	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
	while( sqlite3_step(stmt) != SQLITE_DONE )
	{
		t->id = id;
		strcpy(t->description, sqlite3_column_text(stmt, 1));
		t->priority = sqlite3_column_int(stmt, 2);
		t->status = sqlite3_column_int(stmt, 3);
		t->started = 0;
		t->ended = 0;
		t->id_user = sqlite3_column_int(stmt, 6);
		t->id_owner = sqlite3_column_int(stmt, 7);
		strcpy(t->created, sqlite3_column_text(stmt, 8));
		if( sqlite3_column_text(stmt, 9) )
			strcpy(t->modified, sqlite3_column_text(stmt, 9));
	}

	sqlite3_free(query);
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return t;
}

int set(char *arg1, char *arg2, char *arg3)
{
	int result = 0;
	FTASKPTR t;
	if ((t = malloc(sizeof(FTASK))) != NULL)
	{
		t = select_task(atoi(arg1));
	}
	else {
		printf("Memory is full.\n");
		return 1;
	}

	if( strcmp(arg2,"description") == 0) strcpy(t->description, arg3);
	else if( strcmp(arg2,"priority") == 0) t->priority = atoi(arg3);
	else if( strcmp(arg2,"status") == 0) t->status = atoi(arg3);
	t->id_user = 1;
	t->id_owner = 1;

	char* query;
	if( check_task(t->id) > 0 )
	{
		query = sqlite3_mprintf("UPDATE task SET description='%s',"\
			"priority=%d,status=%d,started='%s',ended='%s',id_user=%d,"\
			"id_owner=%d,modified=datetime(CURRENT_TIMESTAMP,'localtime') "\
			"WHERE id=%d;",
		    t->description, t->priority, t->status, t->started,
			t->ended, t->id_user, t->id_owner, t->id);

		result = exec_query(query);
		sqlite3_free(query);
	}
	printf("\n%d task(s) modified.\n\n", result);
	free(t);
	return 0;
}

int check_task(int id)
{
	int result = 0;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = sqlite3_open_v2(DB, &db, SQLITE_OPEN_READWRITE, NULL);
	DBCHECK;

	char* query = sqlite3_mprintf("SELECT id FROM task WHERE id=%d;", id);

	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
	while( sqlite3_step(stmt) != SQLITE_DONE )
	{
		result = 1;
	}

	sqlite3_free(query);
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return result;
}

static int callback(void *data, int argc, char **argv, char **azColName)
{
   fprintf(stderr, "%s: ", (const char*)data);

   for(int i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int print_readme(void)
{
	FILE *f;
	int ch;
	f = fopen( README, "r" );
	if( f == NULL ) { return (1); }
	while( (ch=fgetc(f)) != EOF )
		putchar(ch);
	fclose(f);
	return (0);
}

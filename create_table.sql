DROP table task;
DROP TABLE user;

CREATE TABLE "task" (
	"id"	INTEGER,
	"description"	TEXT NOT NULL,
	"priority"	INTEGER NOT NULL,
	"status"	INTEGER NOT NULL,
	"started"	TEXT,
	"ended"		TEXT,
	"id_user"	INTEGER,
	"id_owner"	INTEGER,
	"created"	TEXT NOT NULL,
	"modified"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("id_user") REFERENCES "user"("id"),
	FOREIGN KEY("id_owner") REFERENCES "user"("id")
);

CREATE TABLE "user" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL,
	"room"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);

insert into user(name,room) values("Fabio","s1");
